package ui;

import classes.Company;
import classes.CompanyObserver;
import classes.Employee;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class Controller implements Initializable {

    private List<Employee> employees;
    public Button btnAdd;
    public TextField txtName;

    public TextField txtAge;

    @FXML
    public ListView<String> lvEmployees;

    private Company company;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        if(this.company == null){
            this.company = new Company();
            this.company.addObserver(new CompanyObserver(this)); // Observer toevoegen aan company
        }
    }

    public void btnAddEmployee(ActionEvent actionEvent) {
        String name = txtName.getText().toString();
        String age = txtAge.getText().toString();
        int iAge = Integer.parseInt(age);

        this.company.addEmployee(name,iAge);
        //updateEmployeeList();
    }


    public void updateEmployeeList(){
        lvEmployees.getItems().clear();
        for( Employee employee : this.company.getEmployees()){
            String output = "Naam: " + employee.getName() + " Leeftijd: " + employee.getAge();
            lvEmployees.getItems().add(output);
        }
    }
}
