package classes;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

public class Company extends Observable {
    private ArrayList<Employee> Employees;

    public Company(){
        this.Employees = new ArrayList<Employee>();
    }

    public ArrayList<Employee> getEmployees() {
        return Employees;
    }

    public void setEmployees(ArrayList<Employee> employees) {
        Employees = employees;
    }

    public void employeesAdded(){
        setChanged(); // De observers zien dat er een verandering is geweest
        notifyObservers(Employees);
    }

    public void addEmployee(String name, int age){
        this.getEmployees().add(new Employee(name,age));
        employeesAdded();
    }

}
