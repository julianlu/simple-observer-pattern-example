module ObserverPattern {
    requires javafx.controls;
    requires javafx.fxml;

    opens ui;
    exports ui;
}
