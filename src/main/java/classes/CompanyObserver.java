package classes;

import ui.Controller;

import java.util.Observable;
import java.util.Observer;

public class CompanyObserver implements Observer {

    private Controller controller;

    public CompanyObserver(Controller controller){
        this.controller = controller;
    }

    @Override
    public void update(Observable o, Object arg) {
        controller.updateEmployeeList();
    }
}
